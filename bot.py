import discord
import asyncio

from discord.ext import commands
from datetime import datetime

cmd_prefix = ":"

bot = commands.Bot(command_prefix=cmd_prefix, description="Reap users")


# override on_ready
@bot.event
async def on_ready():
    print('/-----------------------------------------------------------------------------')
    print('| # ME')
    print('| Name:     ' + bot.user.name)
    print('| ID:       ' + str(bot.user.id))
    print('| Time:     ' + datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    print('|-----------------------------------------------------------------------------')
    print('| # MODULES')
    bot.load_extension('reap')
    bot.load_extension('owner')
    print('|-----------------------------------------------------------------------------')
    print('| # GUILDS')
    for g in bot.guilds:
        print('|   {}'.format(g.name))
    print('|-----------------------------------------------------------------------------')

if __name__ == '__main__':
    secret = ''
    with open('token.txt', 'r') as fp:
        secret = fp.readline().strip()
    bot.run(secret)