import discord
import asyncio
from discord.ext import commands

class Owner:
    def __init__(self, bot):
        self.bot = bot

        print('| Loaded:   owner')

    @commands.command(description='kills bot')
    @commands.is_owner()
    async def kill(self, ctx, ver: str=''):
        exit(2)


    @commands.command(description='prints bot pid')
    @commands.is_owner()
    async def pid(self, ctx):
        from os import getpid
        await ctx.channel.send(getpid())


    @commands.command(description='prints to console')
    @commands.is_owner()
    async def print(self, ctx, *, a_str: str='nil'):
        print(a_str)


def setup(bot):
    bot.add_cog(Owner(bot))