import discord
import asyncio
from discord.ext import commands
from config import loadconfig
from config import saveconfig

class Reap:
    def __init__(self, bot):
        self.bot = bot

        temp_exempt_list = loadconfig('exempt.json')
        self.exempt_list = {}
        for g in self.bot.guilds:
            if str(g.id) in temp_exempt_list.keys():
                self.exempt_list[g.id] = temp_exempt_list[str(g.id)]
            else:
                self.exempt_list[g.id] = []

        temp_toreap = loadconfig('toreap.json')
        self.toreap = {}
        for g in self.bot.guilds:
            if str(g.id) in temp_toreap.keys():
                self.toreap[g.id] = temp_toreap[str(g.id)]
            else:
                self.toreap[g.id] = []

        temp_suggest_list = loadconfig('suggest.json')
        self.suggest_list = {}
        for g in self.bot.guilds:
            if str(g.id) in temp_suggest_list.keys():
                self.suggest_list[g.id] = temp_suggest_list[str(g.id)]
            else:
                self.suggest_list[g.id] = []

        temp_active_reap_list = loadconfig('active_reap.json')
        self.active_reap_list = {}
        for g in self.bot.guilds:
            if str(g.id) in temp_active_reap_list.keys():
                self.active_reap_list[g.id] = temp_active_reap_list[str(g.id)]
            else:
                self.active_reap_list[g.id] = False

        print('| Loaded:   reap')


    def check_mod_perms(ctx):
        return ctx.author.guild_permissions.kick_members


    # check for ending temporary bans
    async def ban_cleanup(self):
        await self.bot.wait_until_ready()

        while not bot.is_closed:
            for g in self.bot.guilds:
                if self.active_reap_list[g.id]:
                    print("### reaping members from {} ###".format(g.name))
                    count = 0
                    for mem in g.members:
                        if mem.id not in self.exempt_list[g.id]:
                            print("kicking member {} with user id: {}".format(mem.name, mem.id))
                            await mem.kick()
                            count += 1
                    print("kicked {} member(s)".format(count))

        await asyncio.sleep(60 * 60 * 24)


    ### managing active reap list ###
    @commands.command(description='activates periodic reap')
    async def activate(self, ctx):
        self.active_reap_list[ctx.guild.id] = True
        await ctx.channel.send("{}: reaping is now {}".format(ctx.author.mention, "ON" if self.active_reap_list[ctx.guild.id] else "OFF"))
        temp = loadconfig('active_reap.json')
        temp[str(ctx.guild.id)] = self.active_reap_list[ctx.guild.id]
        saveconfig(self.active_reap_list, 'active_reap.json')


    @commands.command(description='deactivates periodic reap')
    async def deactivate(self, ctx):
        self.active_reap_list[ctx.guild.id] = False
        await ctx.channel.send("{}: reaping is now {}".format(ctx.author.mention, "ON" if self.active_reap_list[ctx.guild.id] else "OFF"))
        temp = loadconfig('active_reap.json')
        temp[str(ctx.guild.id)] = self.active_reap_list[ctx.guild.id]
        saveconfig(self.active_reap_list, 'active_reap.json')


    @commands.command(description='view if periodic reap is on')
    async def view(self, ctx):
        await ctx.channel.send("{}: reaping is {}".format(ctx.author.mention, "ON" if self.active_reap_list[ctx.guild.id] else "OFF"))


    ### managing exempt list ###
    @commands.group(pass_context=True)
    @commands.check(check_mod_perms)
    async def exempt(self, ctx):
        if ctx.invoked_subcommand is None:
            pass


    @exempt.command(name='load', description='load exempt list from file')
    async def _exempt_load(self, ctx):
        temp = loadconfig('exempt.json')
        self.exempt_list[ctx.guild.id] = temp[str(ctx.guild.id)]


    @exempt.command(name='save', description='save exempt list to a file')
    async def _exempt_save(self, ctx):
        temp = loadconfig('exempt.json')
        temp[str(ctx.guild.id)] = self.exempt_list[ctx.guild.id]
        saveconfig(self.exempt_list, 'exempt.json')


    @exempt.command(name='list', description='list exempted users')
    async def _exempt_list(self, ctx):
        bld_str = '```\n'
        for uid in self.exempt_list[ctx.guild.id]:
            user = self.bot.get_user(uid)
            bld_str += user.name + ' ' + str(uid) + '\n'
        bld_str += '```'
        await ctx.channel.send("{}: \n{}".format(ctx.author.mention, bld_str))


    @exempt.command(name='add', description='add an exempted user')
    async def _exempt_add(self, ctx, user: discord.Member):
        if user.id not in self.exempt_list[ctx.guild.id]:
            self.exempt_list[ctx.guild.id].append(user.id)
            await ctx.channel.send("{} user {} added".format(ctx.author.mention, user.name))
        else:
            await ctx.channel.send("{} user {} already added".format(ctx.author.mention, user.name))


    @exempt.command(name='remove', description='remove an exempted user')
    async def _exempt_remove(self, ctx, user: discord.Member):
        if user.id in self.exempt_list[ctx.guild.id]:
            self.exempt_list[ctx.guild.id] = [uid for uid in self.exempt_list[ctx.guild.id] if uid != user.id]
            await ctx.channel.send("{} user {} removed".format(ctx.author.mention, user.name))
        else:
            await ctx.channel.send("{} user {} already removed".format(ctx.author.mention, user.name))


    ### managing toreap list ###
    @commands.group(pass_context=True)
    @commands.check(check_mod_perms)
    async def reap(self, ctx):
        if ctx.invoked_subcommand is None:
            pass


    @reap.command(name='load', description='load reap list from file')
    async def _reap_load(self, ctx):
        temp = loadconfig('toreap.json')
        self.toreap[ctx.guild.id] = temp[str(ctx.guild.id)]


    @reap.command(name='save', description='save reap list to a file')
    async def _reap_save(self, ctx):
        temp = loadconfig('toreap.json')
        temp[str(ctx.guild.id)] = self.toreap[ctx.guild.id]
        saveconfig(self.toreap, 'toreap.json')


    @reap.command(name='list', description='list users to be reaped')
    async def _reap_toreap(self, ctx):
        bld_str = '```\n'
        for uid in self.toreap[ctx.guild.id]:
            user = self.bot.get_user_info(uid)
            bld_str += user.name + ' ' + uid + '\n'
        bld_str += '```'
        await ctx.channel.send("{}: \n{}".format(ctx.author.mention, bld_str))


    @reap.command(name='add', description='add a user to reap')
    async def _reap_addreap(self, ctx, user: discord.Member):
        if user.id not in self.toreap[ctx.guild.id]:
            self.toreap[ctx.guild.id].append(user.id)
            await ctx.channel.send("{} user {} added".format(ctx.author.mention, user.name))
        else:
            await ctx.channel.send("{} user {} already added".format(ctx.author.mention, user.name))


    @reap.command(name='remove', description='remove a user from reap list')
    async def _reap_removereap(self, ctx, user: discord.Member):
        if user.id in self.toreap[ctx.guild.id]:
            self.toreap[ctx.guild.id] = [uid for uid in self.toreap[ctx.guild.id] if uid != user.id]
            await ctx.channel.send("{} user {} removed".format(ctx.author.mention, user.name))
        else:
            await ctx.channel.send("{} user {} already removed".format(ctx.author.mention, user.name))


    ### reaping ###
    @reap.command(name='all', description='reaps all users not on exempt list')
    async def _reap_reapall(self, ctx):
        count = 0
        for mem in ctx.guild.members:
            if mem.id not in self.exempt_list[ctx.guild.id]:
                print("kicking member {} with user id: {}".format(mem.name, mem.id))
                await mem.kick()
                count += 1
        await ctx.channel.send("kicked {} member(s)".format(count))


    @reap.command(name='select', description='reaps all users not on exempt list')
    async def _reap_reapselect(self, ctx):
        count = 0
        for uid in self.toreap[ctx.guild.id]:
            if uid not in self.exempt_list[ctx.guild.id]:
                print("kicking member with user id: {}".format(uid))
                await mem.kick()
                count += 1
        await ctx.channel.send("kicked {} member(s)".format(count))


    @reap.command(name='all_test', description='reaps all users not on exempt list')
    async def _reap_reaptest(self, ctx):
        count = 0
        for mem in ctx.guild.members:
            if mem.id not in self.exempt_list[ctx.guild.id]:
                print("test kicking member {} with user id: {}".format(mem.name, mem.id))
                count += 1
            else:
                print("test not kicking member {} with user id: {}".format(mem.name, mem.id))
        await ctx.channel.send("test kicked {} member(s)".format(count))


    @reap.command(name='show', description='reaps all users not on exempt list')
    async def _reap_reapshow(self, ctx):
        count = 0
        bld_str = '```\n'
        for mem in ctx.guild.members:
            if mem.id not in self.exempt_list[ctx.guild.id]:
                bld_str += "{} : {}\n".format(mem.name, mem.id)
                count += 1
            if len(bld_str) >= 1900:
                bld_str += '```'
                await ctx.channel.send("{} member(s) to kick:\n{}".format(count, bld_str))
                bld_str = '```\n'

        bld_str += '```'
        await ctx.channel.send("{} member(s) to kick:\n{}".format(count, bld_str))


    ### suggest list management ###
    @commands.group(pass_context=True)
    @commands.check(check_mod_perms)
    async def suggest(self, ctx):
        if ctx.invoked_subcommand is None:
            pass


    @suggest.command(name='load', description='load reap list from file')
    async def _suggest_load(self, ctx):
        temp = loadconfig('suggest.json')
        self.suggest_list[ctx.guild.id] = temp[str(ctx.guild.id)]


    @suggest.command(name='save', description='save reap list to a file')
    async def _suggest_save(self, ctx):
        temp = loadconfig('suggest.json')
        temp[str(ctx.guild.id)] = self.suggest_list[ctx.guild.id]
        saveconfig(self.suggest_list, 'suggest.json')


    @suggest.command(name='find', description='suggest users who should be exempt')
    async def _suggest(self, ctx):
        count = 0
        for mem in ctx.guild.members:
            if mem.id not in self.exempt_list[ctx.guild.id] and mem.bot:
                self.suggest_list[ctx.guild.id].append(mem.id)
                count += 1
        await ctx.channel.send("{} member(s) to suggest".format(count))


    @suggest.command(name='list', description='list suggested users who should be exempt')
    async def _list_suggest(self, ctx):
        bld_str = '```\n'
        for uid in self.suggest_list[ctx.guild.id]:
            user = self.bot.get_user(uid)
            bld_str += user.name + ' ' + str(uid) + '\n'
        bld_str += '```'
        await ctx.channel.send("{}: \n{}".format(ctx.author.mention, bld_str))


    @suggest.command(name='find_and_list', description='suggest users who should be exemptand list them')
    async def _suggest_and_list(self, ctx):
        count = 0
        for mem in ctx.guild.members:
            if mem.id not in self.exempt_list[ctx.guild.id] and mem.bot:
                self.suggest_list[ctx.guild.id].append(mem.id)
                count += 1

        bld_str = '```\n'
        for uid in self.suggest_list[ctx.guild.id]:
            user = self.bot.get_user(uid)
            bld_str += user.name + ' ' + str(uid) + '\n'
        bld_str += '```'
        await ctx.channel.send("{} member(s) to suggest: \n{}".format(count, bld_str))


    @suggest.command(name='merge', description='list suggested users who should be exempt')
    async def _merge_suggest(self, ctx):
        for uid in self.suggest_list[ctx.guild.id]:
            if uid not in self.exempt_list[ctx.guild.id]:
                self.exempt_list[ctx.guild.id].append(uid)
        await ctx.channel.send("{}: merged".format(ctx.author.mention))


    @commands.command(description='clears all lists')
    async def clear(self, ctx):
        self.exempt_list[ctx.guild.id] = []
        self.toreap[ctx.guild.id] = []
        self.suggest_list[ctx.guild.id] = []


def setup(bot):
    bot.add_cog(Reap(bot))